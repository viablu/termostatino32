/* Copyright © 2019 Samuel Albani

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <WiFiServer.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>

//Pin
#define VENTOLA 33
#define PIN_TEMP 34
#define PIN_POTENZIOMETRO 35
//Pin LED RGB
#define ledR 27
#define ledG 26
#define ledB 25

//Parametri termistore
#define V_REF 3.343 //Volt
#define R_SERIE_PART 5000 //Valore della resistenza in Ohm in serie a quella termica come partitore di tensione
#define BETA 3950 //Parametro caratteristico del termistore NTC

#define PWM_RES 4 //Risoluzione PWM in bit
#define SOGLIA 2.0 //Quando la temperatura desiderata è <= a quella rilevata-SOGLIA, parte la ventola

//Credenziali WiFi
const char* wifi_ssid = "";
const char* wifi_password = "";
/* Possibili broker MQTT:
mqtt.hacklabcormano.it
iot.eclipse.org
test.mosquitto.org
broker.hivemq.com
mqtt.fluux.io
*/
#define mqtt_server "mqtt.hacklabcormano.it"
#define mqtt_porta 1883
#define mqtt_topic "Termostatino32"
#define mqtt_subtopic_tempdes "imposta"

WiFiClient espClient;
PubSubClient client(espClient);
char msg[20];
bool tempDes_mqtt=false;  //vero se tempDes impostata tramite MQTT, falso se impostata con potenziometro
double tempDes;

void setup() {
  Serial.begin(9600);
  setup_wifi();
  randomSeed(micros());
  client.setServer(mqtt_server, mqtt_porta);
  client.setCallback(callback);

  ledcSetup(4, 10, PWM_RES); // Ventola: 10 Hz PWM
  ledcAttachPin(VENTOLA, 4); // assign VENTOLA pin to channel

  ledcAttachPin(ledR, 1); // assign RGB led pins to channels
  ledcAttachPin(ledG, 2);
  ledcAttachPin(ledB, 3);

  // Initialize channels 
  // channels 0-15, resolution 1-16 bits, freq limits depend on resolution
  // ledcSetup(uint8_t channel, uint32_t freq, uint8_t resolution_bits);
  ledcSetup(1, 1000, PWM_RES); // LED RGB: 1 kHz PWM
  ledcSetup(2, 1000, PWM_RES);
  ledcSetup(3, 1000, PWM_RES);
}

void loop() {
  if(!tempDes_mqtt)
    tempDes=20.0/4095*analogRead(PIN_POTENZIOMETRO)+20.0;  //Selezione temperatura compresa tra 20 °C e 40 °C
  int sensorValue=analogRead(PIN_TEMP);
  double temp=calcola_temperatura(sensorValue);
  Serial.print(temp);
  Serial.print(", ");
  Serial.print(tempDes);
  int pwm_ventola=calcola_ventola(temp, tempDes);
  ledcWrite(4, pwm_ventola);
  double velocita_ventola_percentuale=pwm_ventola*100.0/(pow(2,PWM_RES)-1);
  Serial.print(", ");
  Serial.println(velocita_ventola_percentuale);

  //Scrittura valori RGB sui pin
  ledcWrite(1, pwm_ventola);
  ledcWrite(2, temp<tempDes?0:pow(2, PWM_RES)-pwm_ventola-1);
  ledcWrite(3, temp>=tempDes?0:15);

  //MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  snprintf(msg, sizeof(msg)/sizeof(*msg), "%lf°C", temp); //sizeof(msg)/sizeof(*msg) è la lunghezza di msg
  Serial.print("Pubblicazione messaggio MQTT: \"Temperatura ");
  Serial.print(msg);
  Serial.println('"');
  client.publish(mqtt_topic"/Temperatura", msg);

  snprintf(msg, sizeof(msg)/sizeof(*msg), "%lf°C", tempDes);
  Serial.print("Pubblicazione messaggio MQTT: \"Temperatura_desiderata ");
  Serial.print(msg);
  Serial.println('"');
  client.publish(mqtt_topic"/Temperatura_desiderata", msg);

  snprintf(msg, sizeof(msg)/sizeof(*msg), "%lf%%", velocita_ventola_percentuale);
  Serial.print("Pubblicazione messaggio MQTT: \"Velocita_ventola_percentuale ");
  Serial.print(msg);
  Serial.println('"');
  client.publish(mqtt_topic"/Velocita_ventola_percentuale", msg);

  delay(500);
}

double calcola_temperatura(int sensorValue) {
  double volt=V_REF*sensorValue/4095;
  double resist=(V_REF-volt)*R_SERIE_PART/volt;
  double temp=1.0/(1/298.15+log(resist/10000.0)/BETA)-273.15;
  // print out the value you read:
  /*Serial.print(sensorValue);
  Serial.print(", ");
  Serial.print(volt);
  Serial.print(", ");
  Serial.print(resist);
  Serial.print(", ");
  Serial.print(temp);*/
  return temp;
}

int calcola_ventola(double temp, double temp_des) {
  static bool ventola_accesa=false;
  if(temp<=temp_des) {
    ventola_accesa=false;
    return 0;
  }
  int granularita_pwm=pow(2, PWM_RES);
  if(temp-temp_des>4.0) { //Range PWM di 4 °C
    ventola_accesa=true;
    return granularita_pwm-1;
  }
  if(ventola_accesa || temp-temp_des>=SOGLIA) {
    ventola_accesa=true;
    return (temp-temp_des)/4.0*(granularita_pwm-1);
  }
  return 0;
}

//------------------- GESTIONE DELLA RETE ------------------

void setup_wifi() {
  Serial.println();
  Serial.print("Connessione a ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("\nWiFi connesso");
  Serial.println("indirizzo IP: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Tentativo connessione MQTT broker...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("Broker MQTT connesso");
      client.subscribe(mqtt_topic"/"mqtt_subtopic_tempdes);
    } else {
      Serial.print("fallito, rc=");
      Serial.print(client.state());
      Serial.println(" nuovo tentativo in 2 seconds");
      delay(2000);
    }
  }
}

void callback(const char* topic, byte* payload, unsigned int length) {
  Serial.print("Messaggio arrivato [");
  Serial.print(topic);
  Serial.print("] ");
  if(length>0) {
    short i;
    for (i=0; i<length && i<sizeof(msg)/sizeof(*msg)-1; i++)
      msg[i]=(char)payload[i];
    msg[i]='\0';
    Serial.print(msg);
    Serial.println("°C");
    tempDes_mqtt=true;
    tempDes=atof(msg);
  }
  else {
    tempDes_mqtt=false;
    Serial.println();
  }
}
